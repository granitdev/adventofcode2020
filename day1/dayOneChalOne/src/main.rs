use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let numbers = read_num_list("./input.sql");

    // solution_one(numbers);
    solution_two(numbers);
}

fn read_num_list(file_name: &str) -> Vec<u32>{
    let file = File::open(file_name).expect("file not found!");
    let reader = BufReader::new(file);
    let mut vec: Vec<u32> = Vec::new();

    for line in reader.lines() {
        match line {
            Ok(linevalue) => vec.push(linevalue.parse::<u32>().unwrap()),
            Err(why) => println!("{}", why),
        }
    }
    vec
}

fn solution_one(numbers: Vec<u32>) {
    for num in &numbers {
        for num2 in &numbers {
            if num != num2 && num + num2 == 2020 {
                println!("Item: {}", num);
                println!("Add: {}", num2);
                println!("\t{}", num+num2);
                println!("\t{}", num*num2);
            }
        }
    }
}

fn solution_two(numbers: Vec<u32>) {
    for num in &numbers {
        for num2 in &numbers {
            for num3 in &numbers {
                if num != num2 && num2 != num3 &&
                   num + num2 + num3 == 2020 {
                    println!("Num1: {}", num);
                    println!("Num2: {}", num2);
                    println!("Num3: {}", num3);
                    println!("\t{}", num + num2 + num3);
                    println!("\t{}", num * num2 * num3);
                }
            }
        }
    }
}