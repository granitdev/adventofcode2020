use std::fs;

struct Password {
    min: u8,
    max: u8,
    letter: String,
    value: String,
}

fn main() {
    let file = "/home/brandon/development/adventofcode2020/day2/test_input";
    let input = fs::read_to_string(file).unwrap();
    let lines: Vec<&str> = input.split("\n").collect();

    for line in lines {
        createPassword(line);
    }
}

fn createPassword(line: &str) {
    println!("{}", line);
    let halves: Vec<&str> = line.split(":").collect();
    println!("Password: {}", halves[1].trim());

    // println!("Letter: {}", halves[0].chars().last().unwrap());

    // let first = String::from(halves[0]);
    // let minmax: Vec<&str> = first[..first.len()-2].split("-").collect();
    // println!("Min: {}\nMax: {}", minmax[0].parse::<u8>().unwrap(), minmax[1].parse::<u8>().unwrap())

    // Password {
    //     min: minmax[0].parse::<u8>().unwrap(),
    //     max: minmax[2].parse::<u8>().unwrap(),
    //     letter: String::from(halves[0].chars().last().unwrap()),
    //     value: String::from(halves[1].trim()),
    // }
}