use std::fs;

struct Password {
    min: usize,
    max: usize,
    letter: String,
    value: String,
}

fn main() {
    println!("Part One: {}", part_one_answer());
    println!("Part Two: {}", part_two_answer());
}

fn part_two_answer() -> usize {
    let file = "/home/brandon/development/adventofcode2020/day2/input";
    let input = fs::read_to_string(file).unwrap();
    let lines: Vec<&str> = input.split("\n").collect();
    let mut pws: Vec<Password> = Vec::new();
    
    for line in lines {
        if line.len() > 0 {
            pws.push(create_password(line));
        }
    }

    pws.iter().filter(|pw| validate_password_2(&pw)).count()
}

fn part_one_answer() -> usize {
    let file = "/home/brandon/development/adventofcode2020/day2/input";
    let input = fs::read_to_string(file).unwrap();
    let lines: Vec<&str> = input.split("\n").collect();
    let mut pws: Vec<Password> = Vec::new();
    
    for line in lines {
        if line.len() > 0 {
            pws.push(create_password(line));
        }
    }

    pws.iter().filter(|pw| validate_password(&pw)).count()
}

fn create_password(line: &str) -> Password
{
    let halves: Vec<&str> = line.split(":").collect();
    let first = String::from(halves[0]);
    let letter = &first[first.len()-1..];
    let minmax: Vec<&str> = first[..first.len()-2].split("-").collect();

    Password {
        min: minmax[0].parse::<usize>().unwrap(),
        max: minmax[1].parse::<usize>().unwrap(),
        letter: String::from(letter),
        value: String::from(halves[1].trim()),
    }
}

fn validate_password(pw: &Password) -> bool {
    let count = pw.value.matches(&pw.letter).count();
    if count >= pw.min && count <= pw.max {
        return true;
    }
    return false;
}

fn validate_password_2(pw: &Password) -> bool {
    if pw.min-1 >= pw.value.len() || pw.max-1 >= pw.value.len() {
        return false;
    }
    let first: bool = pw.value.as_bytes()[pw.min - 1] == pw.letter.as_bytes()[0];
    let second: bool = pw.value.as_bytes()[pw.max - 1] == pw.letter.as_bytes()[0];
    if first && !second {
        return true;
    }
    if !first && second {
        return true;
    }
    return false;
}